function createCard(name, description, pictureUrl, newStart, newEnd, location) {
    return `
      <div class="col card m-1 shadow p-3 mb-5 bg-body-tertiary rounded">
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
          <h5 class="card-title">${name}</h5>
          <h6 class="card-subtitle mb-2 text-muted">${location}</h6>
          <p class="card-text">${description}</p>
        </div>
        <div class="card-footer">
        ${newStart} to ${newEnd}
        </div>
      </div>
    `;
  }
function errorMessage(error) {
    return `
    <head>
    <title>${error}</title>
    </head>
    <div class="alert alert-danger" role="alert">
    Alert—check it out!
    </div>
    `
}

  window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';

    try {
      const response = await fetch(url);

      if (!response.ok) {
        return `
                <div class="alert alert-danger" role="alert">
                Alert—There was an error with the response- check it out!
                </div>
                `
      } else {
        const data = await response.json();

        for (let conference of data.conferences) {
          const detailUrl = `http://localhost:8000${conference.href}`;
          const detailResponse = await fetch(detailUrl);
          if (detailResponse.ok) {
            const details = await detailResponse.json();
            const title = details.conference.name;
            const description = details.conference.description;
            const pictureUrl = details.conference.location.picture_url;
            const startDate = new Date(details.conference.starts);
            const newStart = startDate.toDateString();
            const endDate = new Date(details.conference.ends);
            const newEnd = endDate.toDateString();
            const location = details.conference.location.name;
            console.log(location)
            const html = createCard(title, description, pictureUrl, newStart, newEnd, location);
            const column = document.querySelector('.row');
            column.innerHTML += html;


          }
        }

      }
    } catch (e) {
        console.error(e);
      // Figure out what to do if an error is raised
    }

  });
